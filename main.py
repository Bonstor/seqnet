import torchvision.models as models
from torchvision import transforms
import torch
import matplotlib.pyplot as plt
from glob import glob
import numpy as np
from torch.autograd import Variable
from torch import nn
from cv2 import dnn_superres
import segmentation_models_pytorch as smp
import cv2
from PIL import Image, ImageChops
import torch.nn.functional as F


class BB_model(nn.Module):
    def __init__(self):
        super(BB_model, self).__init__()
        resnet = models.resnet34(pretrained=True)
        layers = list(resnet.children())[:8]
        self.features1 = nn.Sequential(*layers[:6])
        self.features2 = nn.Sequential(*layers[6:])
        self.bb = nn.Sequential(nn.BatchNorm1d(512), nn.Linear(512, 4))

    def forward(self, x):
        x = self.features1(x)
        x = self.features2(x)
        x = F.relu(x)
        x = nn.AdaptiveAvgPool2d((1,1))(x)
        x = x.view(x.shape[0], -1)
        return self.bb(x)


def remove_alpha(img):
    b_channel, g_channel, r_channel,alpha = cv2.split(np.array(img))
    b_channel[alpha < 255//2] = 255
    g_channel[alpha < 255//2] = 255
    r_channel[alpha < 255//2] = 255
    img_BGR = cv2.merge((b_channel, g_channel, r_channel))
    img = Image.fromarray(img_BGR)
    return img

class AddCoords(nn.Module):
    def __init__(self, radius_channel=False,cuda=False):
        super(AddCoords, self).__init__()
        self.radius_channel = radius_channel
        self.cuda = cuda

    def forward(self, in_tensor):
        batch_size_tensor = in_tensor.shape[0]

        xx_ones = torch.ones([1, in_tensor.shape[2]], dtype=torch.int32)
        xx_ones = xx_ones.unsqueeze(-1)

        xx_range = torch.arange(in_tensor.shape[2], dtype=torch.int32).unsqueeze(0)
        xx_range = xx_range.unsqueeze(1)

        xx_channel = torch.matmul(xx_ones, xx_range)
        xx_channel = xx_channel.unsqueeze(-1)

        yy_ones = torch.ones([1, in_tensor.shape[3]], dtype=torch.int32)
        yy_ones = yy_ones.unsqueeze(1)

        yy_range = torch.arange(in_tensor.shape[3], dtype=torch.int32).unsqueeze(0)
        yy_range = yy_range.unsqueeze(-1)

        yy_channel = torch.matmul(yy_range, yy_ones)
        yy_channel = yy_channel.unsqueeze(-1)

        xx_channel = xx_channel.permute(0, 3, 1, 2)
        yy_channel = yy_channel.permute(0, 3, 1, 2)

        xx_channel = xx_channel.float() / (in_tensor.shape[2] - 1)
        yy_channel = yy_channel.float() / (in_tensor.shape[3] - 1)

        xx_channel = xx_channel * 2 - 1
        yy_channel = yy_channel * 2 - 1

        xx_channel = xx_channel.repeat(batch_size_tensor, 1, 1, 1)
        yy_channel = yy_channel.repeat(batch_size_tensor, 1, 1, 1)

        if self.cuda:
            out = torch.cat([in_tensor.cuda(), xx_channel.cuda(), yy_channel.cuda()], dim=1)
        else:
            out = torch.cat([in_tensor.cpu(), xx_channel.cpu(), yy_channel.cpu()], dim=1)

        if self.radius_channel:
            radius_calc = torch.sqrt(torch.pow(xx_channel - 0.5, 2) + torch.pow(yy_channel - 0.5, 2))
            out = torch.cat([out, radius_calc], dim=1).cuda()

        return out

class Bbox:
    def __init__(self,model_path='bbox/res34_2.pth',cuda=False):
        normalize = transforms.Normalize(
           mean=[0.485, 0.456, 0.406],
           std=[0.229, 0.224, 0.225]
        )
        self.ds_trans = transforms.Compose([transforms.ToTensor(),normalize,transforms.Resize((224,224))])
        self.model =  BB_model()

        self.model.load_state_dict(torch.load(model_path,map_location=torch.device('cpu')))
        self.model = self.model.cpu()
        if cuda:
            self.model = self.model.cuda()
        self.cuda = cuda
        self.model.eval()

    def _r_normer(self,r,alpha=0.1):
        a = r[1] - r[0]
        a*=alpha
        b = r[3] - r[2]
        b*=alpha
        r = [r[0]-a,r[1]+a,r[2]-b,r[3]+b]
        out = []
        for i in r:
            if i<0:
                out.append(0)
            elif i>1:
                out.append(1)
            else:
                out.append(float(i.cpu()))
        return out

    def predict(self,img):
        #PIL
        batch = self.ds_trans(img).unsqueeze(0)
        if self.cuda:
            batch = batch.cuda()
        else:
            batch = batch.cpu()

        batch = batch.float()
        r = self.model(batch)[0]
        r = self._r_normer(r)

        return r


class Segment_net:
    def __init__(self,model_path='models/dlv3.pth',cuda=False):
        ENCODER = 'resnet50'
        #ENCODER = 'efficientnet-b4'
        ENCODER_WEIGHTS = 'imagenet'
        CLASSES = ['obj']
        ACTIVATION = 'sigmoid' # could be None for logits or 'softmax2d' for multicalss segmentation
        DEVICE = 'gpu'



        self.model = smp.DeepLabV3Plus(
            encoder_name=ENCODER,        # choose encoder, e.g. mobilenet_v2 or efficientnet-b7
            encoder_weights=ENCODER_WEIGHTS,     # use `imagenet` pretreined weights for encoder initialization
            in_channels=3,                  # model input channels (1 for grayscale images, 3 for RGB, etc.)
            classes=1,
            #decoder_attention_type='scse',
            activation='sigmoid',
            #decoder_channels=(256*2, 128*2, 64*2, 32*2, 16*2)
        )

        self.preprocessing_fn = smp.encoders.get_preprocessing_fn(ENCODER, ENCODER_WEIGHTS)

        self.model.load_state_dict(torch.load(model_path,map_location=torch.device('cpu')))

        if cuda:
            self.model = self.model.cuda()
        else:
            self.model = self.model.cpu()

        self.cuda = cuda
        self.model.eval()


    def _preprocess(self, pil_img, scale,image=True):
        w, h = pil_img.size
        newW, newH = int(scale * w), int(scale * h)
        assert newW > 0 and newH > 0, 'Scale is too small'
        pil_img = pil_img.resize((newW, newH))

        img_nd = np.array(pil_img)

        if len(img_nd.shape) == 2:
            img_nd = np.expand_dims(img_nd, axis=2)

        # HWC to CHW
        if image:
            img_nd = self.preprocessing_fn(img_nd)
        img_trans = img_nd.transpose((2, 0, 1))

        return img_trans


    def predict(self,img):
        size = img.size
        img = img.resize((640,640))
        prc = torch.from_numpy(np.array([self._preprocess(img, 1)],dtype=np.float32))

        with torch.no_grad():
            mask = self.model(prc)[0][0]

        mask = mask * 255
        mask = mask.detach().numpy()
        mask = mask.astype(np.uint8)
        blur = cv2.blur(mask,(5,5))
        b_channel, g_channel, r_channel = cv2.split(np.array(img))
        img_BGRA = cv2.merge((b_channel, g_channel, r_channel, blur))
        img = Image.fromarray(img_BGRA).resize(size)
        #img = remove_alpha(img)
        return img

class ResizeModel:
    def __init__(self,model_path='models/FSRCNN_x2.pb'):
        self.sr = dnn_superres.DnnSuperResImpl_create()
        self.sr.readModel(model_path)
        self.sr.setModel("fsrcnn", 2)

    def predict(self,img,size):
        #cv2
        img = np.array(img)
        size = (size[0],size[1])
        while min(img.shape[:-1]) < min(size):
            img = self.sr.upsample(img)

        img = cv2.resize(img,dsize=size,interpolation=cv2.INTER_CUBIC)
        return Image.fromarray(img)


class ImageProcessor:
    def __init__(self,seg_model_path='models/dlv3.pth',
                bbox_model_path='models/res34_2.pth',resize_model_path='models/FSRCNN_x2.pb',cuda=False):
        self.bb_model = Bbox(model_path=bbox_model_path, cuda=cuda)
        self.seq_model = Segment_net(model_path=seg_model_path, cuda=cuda)
        self.up_sample = ResizeModel(model_path=resize_model_path)

    def _crop_image_to_aspect_ratio(self,img,new_wide=3,new_height=4):

        def trim(im):
            bg = Image.new(im.mode, im.size, im.getpixel((0,0)))
            diff = ImageChops.difference(im, bg)
            diff = ImageChops.add(diff, diff, 2.0, -100)
            bbox = diff.getbbox()
            if bbox:
                return im.crop(bbox)

        r = self.bb_model.predict(img)
        wide, height = img.size
        h_min = int(r[0]*height)
        h_max = int(r[1]*height)
        w_min = int(r[2]*wide)
        w_max = int(r[3]*wide)

        wide, height = w_max-w_min,h_max-h_min

        w_c = 0
        h_c = 0
        if wide/height < new_wide/new_height:
            r_wide = int(new_wide*height/new_height)
            w_c = (r_wide - wide)/2

        if wide/height > new_wide/new_height:
            r_height = int(wide*new_height/new_wide)
            h_c = (r_height - height)/2

        #return img.crop([new_w_min,new_h_min,new_w_max,new_h_max])
        return img.crop([w_min-w_c,h_min-h_c,w_max+w_c,h_max+h_c])


    def image_process(self,img,size=None,use_segmentation=False):
        if np.array(img).shape[-1] == 4:
            img = remove_alpha(img)

        if size:
            img = self._crop_image_to_aspect_ratio(img,size[0],size[1])
            img = self.up_sample.predict(img,size)


        if use_segmentation:
            img = self.seq_model.predict(img)

        return img

    def images_process(self,imgs):
        result = []
        for img in imgs:
            result.append(self.image_process(**img))


        return result



if __name__ == '__main__':
    processor = ImageProcessor()
    img = cv2.imread('test.jpg')
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = Image.fromarray(img)
    test_imgs = [{"img":img,"size":(500,500),"use_segmentation":True},
                {"img":img,"size":(500,500),"use_segmentation":False},
                {"img":img,"size":None,"use_segmentation":True},
                {"img":img,"size":None,"use_segmentation":False}]
    res = processor.images_process(test_imgs)
    i=0
    for r in res:
        r.save(f'{i}.png')
        i+=1
    #img.save('1.png')

